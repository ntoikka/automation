#!/usr/bin/env groovy
package examplectrl.vars // Change 'examplectrl' to something specific for each subsystem, e.g. 'dtctrl'

class GlobalVars{
    static def setenv(script, jobname)
    {   
        // Edit this with your subsystem variables
        switch(jobname) {
            case ~/.*-prod$/:
                script.env.rctrl_project = "rctrl-prod"  
                script.env.image = "/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/your-image:prod" // Your unpacked docker image  
                script.env.image_setup = "source /home/[YOUR-SERVICE-ACCOUNT]/setup.sh; module load lxbatch/tzero" // Your image setup script
                script.env.dbinstance = "automation_test" // The InfluxDB where you write - usually different between prod and repro
                script.env.campaign = "prompt" // The campaign must be the same as the one created from https://ecalautomation.docs.cern.ch/data-reprocessing/#creating-a-new-campaign
                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/kbdustfxdjyg3b88d96k3xwyoe" // The mattermost hook for notification - optional functionality
                script.env.eospath = "/eos/cms/store/group/example" // Where you want to save stuff etc...
                script.env.eosplots = "/eos/your-plots" // Where you want to save plots etc... - optional functionality
                script.env.plotsurl = "https://your-plots.web.cern.ch/" // URL were the plots are plublished - optional functionality
                break
            case ~/.*-repro$/:
                script.env.image = "/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/your-image:dev"
                script.env.image_setup = "source /home/[YOUR-SERVICE-ACCOUNT]/setup.sh"
                script.env.dbinstance = "automation_test"
                script.env.campaign = "all"
                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/kbdustfxdjyg3b88d96k3xwyoe"
                script.env.eospath = "/eos/cms/store/group/example"
                script.env.eosplots = "/eos/your-plots"
                script.env.plotsurl = "https://your-plots.web.cern.ch/"
                break
            default:
                script.env.rctrl_project = "rctrl"
                script.env.image = "/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/your-image:dev"
                script.env.image_setup = "source /home/[YOUR-SERVICE-ACCOUNT]/setup.sh"
                script.env.dbinstance = "automation_test"
                script.env.campaign = "prompt"
                script.env.notify_url = "https://mattermost.web.cern.ch/hooks/y95qhxb5a7r49b5m818hjgqssh"
                script.env.eospath = "/eos/cms/store/group/example"
                script.env.eosplots = "/eos/your-plots"
                script.env.plotsurl = "https://your-plots-dev.web.cern.ch/"
                break
        }
    }
}
