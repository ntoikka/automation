#!/usr/bin/env groovy

def call(String script='', String image=env.image, String setup=env.image_setup) {
    withCredentials([usernamePassword(credentialsId: 'jecpcl', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
        // Wrap the script into a singularity exec
        s_script = "echo '$PASSWORD' | kinit jecpcl; apptainer exec -B /cvmfs -B /eos -B /afs -B /etc/sysconfig/ngbauth-submit $image /bin/bash -c 'echo '$PASSWORD' | kinit jecpcl; $setup; $script'" 
        sh(script: s_script)
    }
}

